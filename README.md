# NEAR callables enumerator

This is a simple python module that should enumerate functions callable on the NEAR blockchain in projects using `near-sdk-rs`.

There might be some bugs or missing functionalities, as this was developed quickly and ad-hoc.

## Usage

Install via:

```bash
pip3 install .
```

Then:

```bash
python3 -m near_enumerator # this runs in the current working directory
python3 -m near_enumerator --help # displays help with instructions on how to specify the project path if it is not CWD and if you want to save results to file
```
