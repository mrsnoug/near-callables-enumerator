from setuptools import setup

setup(
    name="near_callable_enumerator",
    version="0.0.1",
    description="Tool for enumerating functions callable onchain in NEAR contracts developed with NEAR-SDK-RS",
    url="#",
    author="Michal Bajor",
    author_email="bajor.michal98@gmail.com",
    licence="MIT",
    packages=["near_enumerator"],
    zip_safe=False,
)
