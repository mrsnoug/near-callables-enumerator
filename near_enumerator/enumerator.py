from copyreg import constructor
import re
import os
from typing import List, Tuple
import json


class Enumerator:
    default_code_pattern = r"#\[near_bindgen]\simpl.+{\n[\s\S]+?}\s}"
    default_exclude_dirs = ["node_modules", ".git", ".idea", ".vscode", "target"]

    def __init__(self, root_path: str = ""):
        self.root_path = root_path
        self.rust_files = []
        self.code_pattern = self.default_code_pattern
        self.exclude_dirs = self.default_exclude_dirs
        self.DEBUG = False

    def set_root_path(self, root_path: str) -> None:
        self.root_path = root_path

    def set_code_pattern(self, code_pattern: str) -> None:
        self.code_pattern = code_pattern

    def set_excluded_dirs(self, excluded_dirs: List[str]) -> None:
        self.excluded_dirs = excluded_dirs

    def set_debug(self, debug: bool) -> None:
        self.DEBUG = debug

    def _function_signature_from_line(
        self, line: str, code_lines: List[str], first_line_index: int
    ) -> str:
        if ")" not in line:
            # multiline function definition, i.e., parameters are in the following lines
            function_signature_end_index = first_line_index + 1
            while ")" not in code_lines[function_signature_end_index]:
                function_signature_end_index += 1

            new_line = ""
            for idx in range(first_line_index, function_signature_end_index + 1):
                if new_line.endswith(",") and ")" not in code_lines[idx]:
                    # for display purposes
                    new_line += " "

                if ")" in code_lines[idx] and new_line.endswith(","):
                    # also for display purposes
                    new_line = new_line[:-1]

                new_line += code_lines[idx].strip()

            line = new_line

        function_signature = line[line.find("fn") + 2 :]

        if function_signature.endswith("{"):
            function_signature = function_signature[:-1]

        return function_signature.strip()

    def _split_views_and_calls(
        self, function_signatures: List[str]
    ) -> Tuple[List[str], List[str], List[str]]:
        views = [func for func in function_signatures if "&self" in func]
        calls = [func for func in function_signatures if "&mut self" in func]
        constructors = [func for func in function_signatures if "-> Self" in func]
        num_of_constructors = len(constructors)

        if (
            len(function_signatures) != len(calls) + len(views) + num_of_constructors
            and self.DEBUG
        ):
            print("ERROR")
            print(f"All_functions: {function_signatures}")
            print(f"Views: {views}")
            print(f"Calls: {calls}")

        return views, calls, constructors

    def _find_files(self) -> None:
        if not self.root_path:
            raise ValueError("No project root set")

        for root, dirs, files in os.walk(self.root_path, topdown=True):
            if self.DEBUG:
                print(f"Dirs found before sanitization: {dirs}")

            dirs[:] = [d for d in dirs if d not in self.exclude_dirs]

            for file in files:
                if file.endswith(".rs"):
                    full_file_path = os.path.join(root, file)
                    self.rust_files.append(full_file_path)

    def enumerate(
        self,
        walk: bool = True,
        save_to_file: bool = True,
        save_filename: str = "callable_functions.json",
    ) -> None:
        if self.DEBUG:
            print("WARNING! Running with DEBUG=True")

        if walk:
            self._find_files()

        if not self.rust_files:
            raise ValueError("NO RUST FILES FOUND. Did you execute with walk=True?")

        if self.DEBUG:
            print(f"Enumerating functions in following files: {self.rust_files}")

        callable_functions = []
        results_per_file = {}
        view_functions = []
        call_functions = []
        constructor_functions = []
        for rust_file in self.rust_files:
            this_file_callable_functions = []

            if self.DEBUG:
                print(f"Working on {rust_file}...")

            with open(rust_file, "r") as read_file:
                code = read_file.read()

            interesting_code_blocks = re.findall(self.code_pattern, code)
            for code_block in interesting_code_blocks:
                code_block = code_block.split("\n")
                impl_line = code_block[1]
                code_block_content = code_block[2:]

                is_trait_implementation = False
                if " for " in impl_line:
                    # naive approach, probably another regex would be better here
                    is_trait_implementation = True
                    if self.DEBUG:
                        print("Found trait implementation")

                for line_index in range(len(code_block_content)):
                    # indexes because we sometimes need to check previous lines
                    line = code_block_content[line_index]

                    if not is_trait_implementation:
                        # only pub fn are callable in this case
                        if "pub fn" in line:
                            function_signature = self._function_signature_from_line(
                                line, code_block_content, line_index
                            )
                            this_file_callable_functions.append(function_signature)
                            callable_functions.append(function_signature)
                            if self.DEBUG:
                                print(f"Adding {function_signature}")
                    else:
                        # in this case everything might be callable unless #[private] macro is used
                        if "fn" in line:
                            # shift = 1
                            shift = (
                                2 if "#[payable]" in code_block[line_index + 1] else 1
                            )
                            if (
                                "#[private]"
                                not in code_block_content[line_index - shift]
                            ):
                                # this is also naive. There might a manual assert making sure that it cannot be called by anyone
                                function_signature = self._function_signature_from_line(
                                    line, code_block_content, line_index
                                )
                                this_file_callable_functions.append(function_signature)
                                callable_functions.append(function_signature)
                                if self.DEBUG:
                                    print(f"Adding {function_signature}")

            file_name = os.path.basename(rust_file)
            views, calls, constructors = self._split_views_and_calls(
                this_file_callable_functions
            )
            this_results = {"view": views, "call": calls, "constructors": constructors}
            results_per_file[file_name] = this_results

            view_functions.extend(views)
            call_functions.extend(calls)
            constructor_functions.extend(constructors)

        if save_to_file:
            with open(save_filename, "w") as write_file:
                json.dump(results_per_file, write_file)

        print("\nConstructors:")
        for func in constructor_functions:
            print(f"    {func}")

        print("\nView functions:")
        for func in view_functions:
            print(f"    {func}")

        print("\nCall functions:")
        for func in call_functions:
            print(f"    {func}")

        print("\nDeveloped with <3 by Michal Bajor")
