import argparse
from .enumerator import Enumerator


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Scan rust files in NEAR project to find functions callable on the blockchain. Developed with <3 by Michal Bajor"
    )
    parser.add_argument(
        "--project-path",
        default=".",
        type=str,
        help="Specify the root of the NEAR project. By default it uses the current working directory",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Specify if you want to print debug info. Useful mostly for development",
    )
    parser.add_argument(
        "--save",
        action="store_true",
        help="Use if you want to save json file with results (callable functions are printed to terminal regardless)",
    )
    parser.add_argument(
        "--save-filename",
        type=str,
        default="callable_functions.json",
        help="Specify filename for the file saved when --save is used. The default name used is callable_functions.json",
    )

    args = parser.parse_args()
    enumerator = Enumerator(root_path=args.project_path)
    enumerator.set_debug(debug=args.debug)
    enumerator.enumerate(save_to_file=args.save, save_filename=args.save_filename)
